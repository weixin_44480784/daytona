## daytona prebuild update

Update a prebuild configuration

```
daytona prebuild update [flags]
```

### Options

```
      --run   Run the prebuild once after updating it
```

### Options inherited from parent commands

```
      --help            help for daytona
  -o, --output string   Output format. Must be one of (yaml, json)
```

### SEE ALSO

* [daytona prebuild](daytona_prebuild.md)	 - Manage prebuilds

