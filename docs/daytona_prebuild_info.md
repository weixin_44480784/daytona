## daytona prebuild info

Show prebuild configuration info

```
daytona prebuild info [flags]
```

### Options inherited from parent commands

```
      --help            help for daytona
  -o, --output string   Output format. Must be one of (yaml, json)
```

### SEE ALSO

* [daytona prebuild](daytona_prebuild.md)	 - Manage prebuilds

