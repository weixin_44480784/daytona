## daytona build info

Show build info

```
daytona build info [BUILD] [flags]
```

### Options inherited from parent commands

```
      --help            help for daytona
  -o, --output string   Output format. Must be one of (yaml, json)
```

### SEE ALSO

* [daytona build](daytona_build.md)	 - Manage builds

